
truncate table yesterday_Members;
insert into yesterday_Members select * from today_Members;
truncate table today_Members;
truncate table new_Members;
truncate table deleted_Members;
truncate table modified_Members;
truncate table sent_for_validation;
truncate table sent_for_transformation;
truncate table transformed_data;

alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
commit;

/*Copy from source the member data from today*/
INSERT INTO today_members SELECT * FROM taMember;
commit;

/*Comparing the members from yesterday with the ones today and finding the NEW added members*/
insert into new_members
select * from today_members where memberno
in
 (
   select memberno from today_members
   minus
  select memberno from yesterday_Members
 );
 
 commit;
 
/*Comparing the members from yesterday with the ones today and finding the DELETED members*/
 insert into deleted_members
 select * from yesterday_Members where memberno 
 in
 (
 select memberno from yesterday_Members
 minus
 select memberno from today_Members
 )
;

commit;

/*Comparing the members from yesterday with the ones today and finding the members that have some of their data modified*/
insert into modified_members
select * from 
(
 select * from today_Members
 minus
 select * from yesterday_Members
 ) 
 where not memberno in
 ( select memberno from today_Members
 minus
 select memberno from yesterday_Members
 )
;

commit;

/*Sending the NEW, MODIFIED and DELETED members for validation */
insert into sent_for_validation select memberno,initials,name,address,zipcode,dateborn,datejoined,dateleft,ownsplanereg,statusstudent,statuspilot,statusascat,statusfullcat,sex,club,'New' from new_members;
insert into sent_for_validation select memberno,initials,name,address,zipcode,dateborn,datejoined,dateleft,ownsplanereg,statusstudent,statuspilot,statusascat,statusfullcat,sex,club,'Deleted' from deleted_members;
insert into sent_for_validation select memberno,initials,name,address,zipcode,dateborn,datejoined,dateleft,ownsplanereg,statusstudent,statuspilot,statusascat,statusfullcat,sex,club,'Modified' from modified_members;

commit;

/*Validating Fixing and Rejecting data and inserting them into the coresponding tables*/
declare 
  cursor m is select * from sent_for_validation;
  MemberNo      number(6,0):=0;
  Initials      char(03):='';
  Name          varchar2(50):='';
  Address       varchar2(50):='';
  ZipCode       number(4,0):=0;
  DateBorn      date:=sysdate;
  DateJoined    date:=sysdate;
  DateLeft      date:=sysdate;
  OwnsPlaneReg  char(03):='';
  StatusStudent char(01):='';
  StatusPilot   char(01):='';
  StatusAsCat   char(01):='';
  StatusFullCat char(01):='';
  Sex           char(01):='';
  Club          varchar2(50):='';
  change_type   varchar2(7):='';
  status        number(1,0):=0; /*0 not changed send it for transfoemation, 1 send to rejected , 2 send to fix send it for transfoemation*/
  counter       number(1,0):=0;
begin 
  for x in m loop
    status   :=0;
    counter  :=0;
    MemberNo :=x.memberno;
    Initials :=x.initials;
    Name     :=x.name;
    Address  :=x.address;
    ZipCode  :=x.zipcode;
    DateBorn :=x.dateborn;
    DateJoined:=x.datejoined;
    DateLeft :=x.dateleft;
    OwnsPlaneReg  :=x.ownsplanereg;
    StatusStudent :=x.statusstudent;
    StatusPilot  :=x.statuspilot;
    StatusAsCat  :=x.statusascat;
    StatusFullCat :=x.statusfullcat;
    Sex           :=x.sex;
    Club          :=x.club;
    change_type   :=x.change_type;
   
    if DateJoined>sysdate then
       DateJoined:=sysdate;
       status:=2;
    end if;
    if extract(year from DateBorn)<(extract(year from sysdate)-2000) then
      DateBorn:=add_months(DateBorn,24000);
      status:=2;
    else if extract(year from DateBorn)<100 then
      DateBorn:=add_months(DateBorn,22800);
      status:=2;
    else if extract(year from DateBorn)<1000 then
      DateBorn:=add_months(DateBorn,12000);
      /*dbms_output.put_line(DateBorn);*/
      status:=2;  
    end if;
    end if;
    end if;
    if StatusFullCat='Y' then counter:=counter+1;
    end if;
    if StatusAsCat='Y' then counter:=counter+1;
    end if;
    if StatusPilot='Y' then counter:=counter+1;
    end if;
    if StatusStudent='Y' then counter:=counter+1;
    end if;
    if counter>1 or counter=0 then
      if StatusFullCat='Y' then 
        StatusAsCat:='N';
        StatusPilot:='N';
        StatusStudent:='N';
        else if StatusAsCat='Y' then
                 StatusPilot:='N';
                 StatusStudent:='N';
                 else if StatusPilot='Y' then
                        StatusStudent:='N';
                          else StatusStudent:='Y';
                      end if;
            end if;
      end if;
      status:=2;
    end if;
    
    if rtrim(Initials) is null then 
      Initials:='???';
      status:=2;
    end if;
    
    if rtrim(Name) is null then 
      Name:='Unkonwn';
      status:=2;
    end if;
    
    if rtrim(Name) is null and Initials='???' then status:=1;
    end if;
    
    if DateBorn >= DateJoined then status:=1;
    end if;
    
    if status=2 then
      insert into fixed_member_data values(MemberNo,Initials,Name,Address,ZipCode,DateBorn,DateJoined,DateLeft,OwnsPlaneReg,StatusStudent,StatusPilot,StatusAsCat,StatusFullCat,Sex,Club,change_type);
      insert into sent_for_transformation values(MemberNo,Initials,Name,Address,ZipCode,DateBorn,DateJoined,DateLeft,OwnsPlaneReg,StatusStudent,StatusPilot,StatusAsCat,StatusFullCat,Sex,Club,change_type);
    end if;
    
    if status=0 then
      insert into sent_for_transformation values(MemberNo,Initials,Name,Address,ZipCode,DateBorn,DateJoined,DateLeft,OwnsPlaneReg,StatusStudent,StatusPilot,StatusAsCat,StatusFullCat,Sex,Club,change_type);
    end if;
    
     if status=1 then
      insert into rejected_member_Data values(MemberNo,Initials,Name,Address,ZipCode,DateBorn,DateJoined,DateLeft,OwnsPlaneReg,StatusStudent,StatusPilot,StatusAsCat,StatusFullCat,Sex,Club,change_type);
    end if;
    
  end loop;
end;
/

commit;

/*Transforming the address, dateleft, plane registration nr and status*/
insert into transformed_data
select memberno,initials,name,
coalesce(trim(address),'UNKNOWN'),  
zipcode,
dateborn,
datejoined,
coalesce(dateleft,to_date('2027-12-31 00:00:00','YYYY-MM-DD HH24:MI:SS')),
coalesce(trim(ownsplanereg),'UNKNOWN'),
case statusFullCat
  when 'Y' then 'Instructor'
  when 'N' then
    case statusAsCat
     when 'Y' then 'Assistant'
     when 'N' then 
      case statusPilot
        when 'Y' then 'Pilot'
        else 'Student'
        end
        end
        end,
case sex
when 'M' then 'Male'
when 'F' then 'Female'
else 'Polygender'
end,
club,
change_type
from sent_for_transformation;

commit;

/*Insering the new members into the member dimension of the DWH*/
insert into d_member(member_key,member_no)
    select sqDWH.nextval,memberno
    from transformed_data
    where change_type='New';

commit;
    
/*Insering the new members into the member profile dimension of the DWH*/
insert into d_memberprofile(member_profile_key,memberno,member_key,initials,membername,address,zipcode,gender,dateofbirth,status,validfrom,validto,currentrow)
      select sqDWH.nextval,t.memberno,m.member_key,t.initials,t.name,t.address,t.zipcode,t.sex,t.dateborn,t.status,sysdate,to_date('2027-12-31 00:00:00','YYYY-MM-DD HH24:MI:SS'),'Valid'
      from transformed_data t
      join d_member m on (t.memberno=m.member_no)
      where t.change_type='New';

commit;

/*Updating the member profile to an invalid profile, for the members that have modified data*/
update d_memberprofile set validto = sysdate, currentrow = 'Invalid' where memberno in (select memberno from transformed_data where change_type = 'Changed');

commit;

/*Inserting into the member profile dimension the members profiles that have been modified and setting them as valid*/
insert into 
     d_memberprofile(member_profile_key,memberno,member_key,initials,membername,address,zipcode,gender,dateofbirth,status,validfrom,validto,currentrow)
     select sqDWH.nextval,t.memberno,m.member_key,t.initials,t.name,t.address,t.zipcode,t.sex,t.dateborn,t.status,sysdate,to_date('2027-12-31 00:00:00','YYYY-MM-DD HH24:MI:SS'),'Valid'
      from transformed_data t
      join d_member m on (t.memberno=m.member_no)
      where t.change_type='Changed';
      
      commit;
      
/*Updating the member profile to an invalid profile, for the members that have been deleted*/
update d_memberprofile set validto = sysdate, currentrow = 'Invalid' where memberno in (select memberno from transformed_data where change_type = 'Deleted');

commit;

