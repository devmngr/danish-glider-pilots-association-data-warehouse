create table last_extraction(
    extractionDate date
                    primary key);

create table newFlights as select * from taflightsvejle where 1=0;
alter table newFlights add club varchar2(50) not null;

create table fixed_Flights as select * from newFlights where 1=0;
create table reject_Flights as select * from newFlights where 1=0;
create table valid_Flights as select * from newFlights where 1=0;

create table transformed_Flights as select * from valid_Flights where 1=0;
alter table transformed_Flights add flight_duration interval day to second not null;
alter table transformed_Flights drop column launchaerotow;
alter table transformed_Flights drop column launchwinch;
alter table transformed_Flights drop column launchselflaunch;
alter table transformed_Flights add launch_type varchar2(12) check(launch_type in ('Aerotow','Winch','Self launch'));

insert into last_extraction values(to_date('2000-12-31 00:00:00','YYYY-MM-DD HH24:MI:SS'));

create table transformedWithKeys as select * from f_flight where 1=0;
alter table transformedWithKeys add pilot1Id number(12,0);
alter table transformedWithKeys add pilot2Id number(12,0);
alter table transformedWithKeys drop column group_id;

insert into d_glider values(-1,-1,-1,'UNKNOWN','Y',1,'UNKNOWN');
commit;