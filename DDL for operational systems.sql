/* ---------------------------------------------- */
/* DDL for creating tables for the operational    */
/* systems for the MUU case used in the Data      */
/* Warehousing course at Vitus Bering             */
/*                                                */
/* NOTE: the script contains references to        */
/* specific tablespaces, that probably need to be */
/* changed before running it                      */
/*                                                */
/*                      Bo Brunsgaard, March 2005 */
/*----------------------------------------------- */
/* revised, February 2011, BBC                    */
/* ---------------------------------------------- */
CREATE TABLE taRegion
  (
    Name VARCHAR2(50) NOT NULL CONSTRAINT coPKtaRegion PRIMARY KEY 
  );
CREATE TABLE taClub
  (
    Mane    VARCHAR2(50) NOT NULL CONSTRAINT coPKtaClub PRIMARY KEY  ,
    Address VARCHAR2(50) NOT NULL ,
    ZipCode CHAR(04) NOT NULL ,
    Region  VARCHAR2(50) CONSTRAINT coFKtaClubtaRegion REFERENCES taRegion
  );
CREATE TABLE taMember
  (
    MemberNo      NUMBER(6,0) NOT NULL CONSTRAINT coPKtaMember PRIMARY KEY ,
    Initials      CHAR(03) NOT NULL ,
    Name          VARCHAR2(50) NOT NULL ,
    Address       VARCHAR2(50) NOT NULL ,
    ZipCode       NUMBER(4,0) NOT NULL ,
    DateBorn      DATE NOT NULL ,
    DateJoined    DATE NOT NULL ,
    DateLeft      DATE ,
    OwnsPlaneReg  CHAR(03) NOT NULL ,
    StatusStudent CHAR(01) NOT NULL CONSTRAINT coCHStatusStudent CHECK (StatusStudent IN ('Y', 'N')) ,
    StatusPilot   CHAR(01) NOT NULL CONSTRAINT coCHStatusPilot CHECK (StatusPilot     IN ('Y', 'N')) ,
    StatusAsCat   CHAR(01) NOT NULL CONSTRAINT coCHStatusErHI CHECK (StatusAsCat      IN ('Y', 'N')) ,
    StatusFullCat CHAR(01) NOT NULL CONSTRAINT coCHStatusFullCat CHECK (StatusFullCat IN ('Y', 'N')) ,
    Sex           CHAR(01) NOT NULL CONSTRAINT coCHSex CHECK (Sex                     IN ('M', 'F')) ,
    Club          VARCHAR2(50) NOT NULL CONSTRAINT coFKtaMemberClub REFERENCES taClub
  );
CREATE TABLE taFlightsVejle
  (
    LaunchTime        DATE NOT NULL ,
    LandingTime       DATE ,
    PlaneRegistration CHAR(03) NOT NULL ,
    Pilot1Init        CHAR(03) NOT NULL ,
    Pilot2Init        CHAR(03) ,
    LaunchAerotow     CHAR(01) CONSTRAINT coCHLaunchAerotow CHECK (LaunchAerotow       IN ('Y' , 'N')) ,
    LaunchWinch       CHAR(01) CONSTRAINT coCHLaunchWinch CHECK (LaunchWinch           IN ('Y' , 'N')) ,
    LaunchSelfLaunch  CHAR(01) CONSTRAINT coCHLaunchSelfLaunch CHECK (LaunchSelfLaunch IN ('Y' , 'N')) ,
    CableBreak        CHAR(01) CONSTRAINT coCHCableBreak CHECK (CableBreak             IN ('Y' , 'N')) ,
    CrossCountryKM    NUMBER(4,0) DEFAULT 0 ,
    CONSTRAINT coPKtaFlightsVejle PRIMARY KEY (LaunchTime, LandingTime, PlaneRegistration)
  );
CREATE TABLE taFlightsSG70 AS
  (SELECT * FROM taFlightsVejle
  );
ALTER TABLE taFlightsSG70 ADD CONSTRAINT coPKtaFlightsSG70 PRIMARY KEY
(
  LaunchTime, LandingTime, PlaneRegistration
);
CREATE TABLE taClass
  (
    Type  VARCHAR2(50) NOT NULL CONSTRAINT coPKtaClass PRIMARY KEY,
    Class VARCHAR2(50) NOT NULL
  );
CREATE TABLE taFly
  (
    Registration  VARCHAR2(10) NOT NULL CONSTRAINT coPKtaFly PRIMARY KEY,
    CompNo        VARCHAR2(2) NOT NULL,
    Type          VARCHAR2(50) CONSTRAINT coFKtaFlyTaClass REFERENCES taClass,
    ClubOwned     NUMBER(1,0) NOT NULL,
    OwnerName     VARCHAR2(50),
    OwnerClubName VARCHAR2(50) NOT NULL,
    HasEngine     NUMBER(1,0) NOT NULL,
    NumberOfSeats NUMBER(1,0) NOT NULL
  );
