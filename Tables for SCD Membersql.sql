create table today_Members as select * from taMember where 1=0;
create table yesterday_Members as select * from taMember where 1=0;
create table new_Members as select * from today_Members where 1=0;
create table deleted_Members as select * from today_Members where 1=0;
create table modified_Members as select * from today_Members where 1=0;
create table sent_for_validation as select * from today_members where 1=0;
alter table sent_for_validation add change_type varchar2(7) not null check (change_type in ('New','Deleted','Changed'));
create table fixed_member_Data as select * from sent_for_validation where 1=0;
create table rejected_member_Data as select * from sent_for_validation where 1=0;
create table sent_for_transformation as select * from sent_for_validation where 1=0;

create table transformed_data (
  memberNo number(6,0) not null,
  initials char(3)not null, 
	name varchar2(50) not null, 
	address varchar2(50) not null, 
	zipcode number(4,0) not null, 
	dateborn date not null, 
	datejoined date not null, 
	dateleft date not null, 
	ownsplanereg varchar2(7) not null, 
	status varchar2(10) not null, 
	sex varchar2(6) not null, 
	club varchar2(50) not null,
	change_type varchar2(8) not null
);

commit;