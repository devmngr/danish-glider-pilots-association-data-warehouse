/*member left before he joined*/
select * 
from tamember
where datejoined>dateleft;

/*number of flights for each plane - we have a plane with ??? reg number that has 12 flights*/
select planeregistration ,count(*)
from taflightsvejle
group by planeregistration
order by count(*);

/*flights where 2 pilots have the same initials*/
select count(*)
from taflightsvejle
where pilot1init=pilot2init;

/*more members with the same initial*/
select initials,count(*)
from tamember
group by initials 
having count(*)>1
;

describe taflightsvejle;

select count (*) from taflightsvejle;

/*same plane in the air in dates that are overlapping*/
select count( *)
from taflightsvejle t1
where exists (select * 
              from taflightsvejle t2
              where t1.planeregistration=t2.planeregistration
              and (t1.launchtime>t2.launchtime and t1.launchtime<t2.landingtime))
             ;

/*status check more then 1 status*/
select distinct statusstudent || statuspilot || statusascat || statusfullcat as stats
from tamember
order by stats;

/*number of members per zipcode*/
select zipcode, count(*)
from tamember
group by zipcode
order by zipcode;

/*check ownsplanereg columns values(its an empty column)*/
select distinct ownsplanereg
from tamember;

/*check flights with landing time null*/
select * 
from taflightsvejle
where landingtime is null;

/*check flights where there is no pilot1*/
select *
from taflightsvejle
where pilot1init not in
(
  select initials 
  from tamember
);

/*check duration of flight is positive*/
select landingtime-launchtime
from taflightsvejle
where landingtime-launchtime<0;

/*maximum and minimum duration of a flight*/
select max(landingtime-launchtime),
      min(landingtime-launchtime)
from taflightsvejle;

/*flight with the minimum duration*/
select * 
from taflightsvejle
where (landingtime-launchtime) in
(     
  select 
  min(landingtime-launchtime)
  from taflightsvejle
);

/*flight with the maximum duration*/
select * 
from taflightsvejle
where (landingtime-launchtime) in
(     
  select 
  max(landingtime-launchtime)
  from taflightsvejle
);

/*search for the flight where the duration is negative*/
select * 
from taflightsvejle
where (landingtime-launchtime)<0;

/*search for a flight where the pilot has left the club*/
/*check for landing time is after the member has left*/
select '(' || pilot1init || ')'
from taflightsvejle
where pilot1init not in
(
  select initials
  from tamember
);

select '(' || pilot2init || ')'
from taflightsvejle
where pilot1init not in
(
  select initials
  from tamember
);

select *
from taflightsvejle
where pilot1init not in
(
  select initials
  from tamember
);

select *
from taflightsvejle
where pilot2init not in
(
  select initials
  from tamember
);
/*COMMAND FOR SETTING THE DATE FORMAT*/
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

select min(launchtime) from taflightsvejle;