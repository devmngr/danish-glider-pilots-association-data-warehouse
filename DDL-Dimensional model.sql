create sequence sqDWH
  start with 1
  increment by 1
  noMaxValue
  cache 20
  ;

create table D_Member
( 
  member_key number(12,0) not null primary key, 
  member_no number(12,0) not null unique
)
;

create table D_MemberProfile
(
   member_profile_key number(12,0)
           primary key        
 , memberNo number(12,0) 
          not null
          references D_Member(member_no)
 , member_key number(12,0)
          not null
          references D_Member(member_key)
 , initials char(3) 
          not null
 , memberName varchar2(50)
          not null
 , address varchar2(50)
          not null
 , zipcode number(4,0)
          not null
 , gender  varchar2(10)
           check (gender in ('Male','Female','Polygender'))
 , dateOfBirth date
          not null
 , status varchar2(20)
            not null
            check (status in ('Student','Pilot','Assistant','Instructor'))
 , validFrom date
             not null           
 , validTo   date
             not null
 , currentRow varchar2(7)
          check ( currentRow in ('Valid','Invalid'))
)
;

create table D_Member_Flight
(
  group_Id_Key number(12,0)
  ,member_profile_key   number(12,0)
  ,weight decimal(2,1)
  ,primary key(group_Id_Key,member_profile_key)
  ,foreign key (member_profile_key) references D_MemberProfile(member_profile_key)
)
;

CREATE TABLE D_Date
(
      date_key number(12,0)
        primary key
       ,date_d date
        not null
      ,year_d number(4,0)
        not null
      ,month_d number(2,0)
        not null
      ,day_d number(2,0)
        not null
      ,hour_d number(2,0)
        not null
      ,minute_d number(2,0)
        not null
      ,dayOfWeek varchar2(10)
        not null
      ,weekNumber number(2,0)
        not null
)
;

create table D_Age
(
  age_key number
    primary key
  ,age number(3)
    not null
)
;

create table D_LaunchType
(
  launch_key number(12,0)
    primary key
  ,cableBreak char(1)
    check ( cableBreak in('Y','N'))
  ,launchType varchar2(20)
    not null
    check (launchType in('Aerotow','Winch','Self launch'))
)
;

create table D_Club
(
  club_key number(12,0)
    primary key
  ,clubName varchar2(50)
    not null
  ,address varchar2(50)
    not null
  ,zipCode number(4,0)
    not null
  ,region varchar2(30)
    not null
)
;

create table D_Glider
(
  glider_key number(12,0)
    primary key
  ,regNum varchar2(10)
    not null
  ,compNum varchar2(2)
    not null
  ,glider_type varchar2(50)
    not null
  ,has_Engine char(1)
    check ( has_Engine in ('Y','N'))
  ,numOfSeats number(1,0)
    check ( numOfSeats in (1,2))
  ,glider_class varchar2(50)
    not null
)
;

create table F_Glider_Ownership
( 
  glider_key number(12,0) 
    not null 
    REFERENCES D_Glider(glider_key)
  ,member_key number(12,0)
    not null 
    REFERENCES D_Member(member_key)
  ,club_key number(12,0) 
    not null
    REFERENCES D_Club(club_key)
  ,ownedFrom number(12,0)
    REFERENCES D_Date(date_key)
  ,ownedTil number(12,0)
    REFERENCES D_Date(date_key)
  ,primary key(glider_key,member_key,club_key,ownedFrom,ownedTil)
)
;

create table F_Flight 
( 
  club_key number(12,0) 
    not null
    references D_Club(club_key)
  ,glider_key number(12,0)
      not null
      references D_Glider(glider_key)
  ,group_id number(12,0) 
      not null 
  ,launchType number(12,0) 
    references D_LaunchType(launch_key)
  ,launchDate number(12,0) 
    references D_Date(date_key)
  ,landingDate number(12,0) 
    references D_Date(date_key)
  ,time_duration number(12,0) 
    not null
  ,crossCountryKm number(7,1)
  ,pilot1age number(12,0) not null
  ,pilot2age number(12,0) not null
  ,primary key(club_key,glider_key,launchType,launchDate,landingDate)
)
;

create table F_Membership
(
  joinDate number(12,0)
    references D_Date(date_key)
  ,leaveDate number(12,0)
    references D_Date(date_key)
  ,club_key number(12,0)
    references D_Club(club_key)
  ,member_key number(12,0)
    references D_Member(member_key)
  ,primary key(joinDate,leaveDate,club_key,member_key)
)
;

commit;

-- -----------------------------------
