truncate table newflights;
truncate table valid_flights;
truncate table transformed_Flights;
truncate table transformedWithKeys;

/*extracting the new flight*/                 
insert into newflights 
    select launchtime,landingtime,planeregistration,pilot1init,pilot2init,launchaerotow,launchwinch,launchselflaunch,cablebreak,crosscountrykm,'Vejle Svæveflyveklub'
    from taflightsvejle
    where launchtime > (select extractionDate from last_extraction);

insert into newflights 
    select launchtime,landingtime,planeregistration,pilot1init,pilot2init,launchaerotow,launchwinch,launchselflaunch,cablebreak,crosscountrykm,'SG70'
    from taflightssg70
    where launchtime > (select extractionDate from last_extraction);
/* Validation  stage*/
update last_extraction set extractionDate=sysdate;
/*PL/SQL cursor for validating data*/

declare 
  cursor flight is
      select * from newflights;
  rejected boolean :=false;
  changed  boolean :=false;
  status varchar2(3):='';
  begin

    for f in flight loop
        rejected :=false;
        changed  :=false;
         status:= f.launchaerotow || f.launchwinch || f.launchselflaunch;
      /*rejecting the flights that have a duration larger than 4 days*/
      if f.landingtime>f.launchtime+4 then rejected:=true;
      end if;
      if f.landingtime<f.launchtime then changed:=true;
                                         f.landingtime:=f.launchtime;
                                         f.launchtime:=f.landingtime;
      end if;
      /*fixing the data where the initials of pilot1 is spaces and pilot2 is valid, just swaping them around*/
      if rtrim(f.pilot1init) is null and rtrim(f.pilot2init) is not null then changed:=true;
                                                                          f.pilot1init:=f.pilot2init;
                                                                          f.pilot2init:=null;
      end if;
      
      if status not in ('YNN', 'NYN', 'NNY') 
            
            then changed:=true;
            if f.launchaerotow='Y' then f.launchwinch:='N';
                                        f.launchselflaunch:='N';
                                else if f.launchselflaunch='Y' then f.launchwinch:='N';
                                                              else if f.launchwinch='N' then f.launchwinch:='Y';
                                                                    end if;
                                    end if;
            end if;
       end if;                           
      if rejected=true then insert into reject_flights 
                              values (f.launchtime,f.landingtime,f.planeregistration,f.pilot1init,f.pilot2init,f.launchaerotow,f.launchwinch,f.launchselflaunch,f.cablebreak,f.crosscountrykm,f.club);
               else 
                insert into valid_flights 
                      values (f.launchtime,f.landingtime,f.planeregistration,f.pilot1init,f.pilot2init,f.launchaerotow,f.launchwinch,f.launchselflaunch,f.cablebreak,f.crosscountrykm,f.club);
                if changed=true then
                        insert into fixed_flights 
                              values (f.launchtime,f.landingtime,f.planeregistration,f.pilot1init,f.pilot2init,f.launchaerotow,f.launchwinch,f.launchselflaunch,f.cablebreak,f.crosscountrykm,f.club);
                end if;
       end if;             
    end loop;
  end;
/


/*transforming the data from the valid_flights and inserting it in the transformed_Flights table*/
/*we are checking the launch type and we are introducing the right type in the transformed_Flights table*/
/*we are calculating the duration of the flight and introducing that in the transformed_Flights table*/
insert into transformed_Flights
select launchtime,landingtime,planeregistration,
pilot1init,pilot2init,cablebreak,crosscountrykm,club,
(landingtime-launchtime) day to second,
case launchaerotow
  when 'Y' then 'Aerotow'
  when 'N' then
    case launchwinch
     when 'Y' then 'Winch'
     when 'N' then 
      case launchselflaunch
        when 'Y' then 'Self launch'
        end
        end
        end
from valid_flights;
/*key look-up*/

insert 
  into transformedWithKeys (club_key, glider_key,launchtype,launchdate,landingdate,time_duration,crosscountrykm,pilot1id,pilot1age,pilot2id,pilot2age)
  
		select coalesce(c.club_key,-1),coalesce(g.glider_key,-1),coalesce(l.launch_key,-1),coalesce(d.date_key,-1),coalesce(d1.date_key,-1),
    extract(day from t.flight_duration)*24*60+extract(hour from t.flight_duration)*60+extract(minute from t.flight_duration),t.crosscountrykm,
    coalesce(m.member_Profile_key,-1),coalesce(a.age_key,-1),coalesce(m1.member_Profile_key,-1),coalesce(a1.age_key,-1)
  
	from transformed_Flights t
    
	left outer join d_glider g
     on  (t.planeregistration = substr(g.regnum,3,3))
  left outer join d_club c
     on  (c.clubname = t.club)
  left outer join d_launchtype l
     on  (l.cablebreak = t.cablebreak and l.launchtype=t.launch_type)
  left outer join D_date d
     on  (d.date_d=t.launchtime)
  left outer join D_date d1
     on  (d1.date_d=t.landingtime)
  left outer join(select extract (year from (sysdate - dateofbirth) year to month) as correctAge,dateofbirth, member_Profile_key, Memberno, initials 
  from d_memberprofile  where initials||' '||dateofbirth in( select initials||' '||younger from
  (select initials, min(dateofbirth) as younger from d_memberprofile where validto = to_date('2027-12-31 00:00:00','YYYY-MM-DD HH24:MI:SS')group by initials))) m
     on  (m.initials=t.pilot1init)
  left outer join (select extract (year from (sysdate - dateofbirth) year to month) as correctAge,dateofbirth, member_Profile_key, Memberno, initials 
  from d_memberprofile  where initials||' '||dateofbirth in( select initials||' '||younger from
  (select initials, min(dateofbirth) as younger from d_memberprofile where validto = to_date('2027-12-31 00:00:00','YYYY-MM-DD HH24:MI:SS')group by initials))) m1
     on  (m1.initials=coalesce(t.pilot2init,'???'))
  left outer join d_age a
     on  (a.age= m.correctAge and m.initials=t.pilot1init)
    left outer join d_age a1
     on  (a1.age=m1.correctAge and m1.initials=coalesce(t.pilot2init,'???'));

DECLARE
  CURSOR x
  IS
    SELECT * FROM transformedwithkeys;
begin
    
  for t in  x loop
      if t.pilot2id =-1 then
         begin
         insert into D_MEMBER_FLIGHT values (sqDWH.nextval,t.pilot1id,1);
         end; 
      else
         begin
           insert into D_MEMBER_FLIGHT values (sqDWH.nextval,t.pilot1id,0.5);
           insert into D_MEMBER_FLIGHT values (sqDWH.currval,t.pilot2id,0.5);
         end;
      end if;
         insert into F_FLight 
         values( t.club_key, t.glider_key ,sqDWH.currval,t.launchtype, t.launchdate, t.landingdate, t.time_duration,
         t.crosscountryKM, t.pilot1age,t.pilot2age);
  end loop;
end;
/
commit;
