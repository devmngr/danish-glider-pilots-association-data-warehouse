alter session set nls_date_format =  'YYYY-MM-DD HH24:MI:SS';

declare

  currentDate date := to_date('2000-01-01 00:00','YYYY-MM-DD HH24:MI');
  lastDate  date := to_date('2027-12-31 23:59','YYYY-MM-DD HH24:MI');
  year      decimal(4,0) := 0;
  month     decimal(2,0) := 0;
  day       decimal(2,0) := 0;
  hour      decimal(2,0) := 0;
  minute    decimal(2,0) := 0;
  dayOfWeek varchar(10) := '';
  weekNumber decimal(2,0) := 0;
  oneMinute   interval day to second := to_dsinterval ('000 00:01:00');
  
begin
  while currentDate <= lastDate loop
     year := extract (year from currentDate);
     month := extract (month from currentDate);
     day := extract (day from currentDate);
     hour :=EXTRACT(HOUR from CAST(currentDate AS TIMESTAMP));
     minute := EXTRACT(minute from CAST(currentDate AS TIMESTAMP));
     weekNumber := to_number(to_char(currentDate,'WW'));
     dayOfWeek := to_char(currentDate,'Day','nls_date_language = English');
     insert into d_date values(sqDWH.nextval,currentDate,year,month,day,hour,minute,dayOfWeek,weekNumber);
     /*dbms_output.put_lne(DayOfWeek || ' ' || to_char(currentDate,'YYYY-MM-DD HH24:MI')|| ' ' ||year|| ' ' ||month|| ' ' ||day|| ' ' ||hour|| ' ' ||minute|| ' week ' || weekNumber);*/
     currentDate := currentDate + oneMinute;
  end loop;
end;
/

/*insert data staticly into D_Glider*/
CREATE TABLE taflyModified AS
  (SELECT * FROM tafly
  );
insert into taflymodified
    select * from tafly;
    
alter table taflymodified add  has_Engine varchar2 (1) check (has_engine in ('Y','N'));
update taflymodified set has_engine ='Y' where hasengine=1;
update taflymodified set has_engine ='N' where hasengine=0;

insert into D_Glider (glider_key,regNum,compnum, glider_type, has_Engine, numOfSeats,glider_class)
select sqDWH.nextval, g.registration, g.compno, g.type, g.has_engine,g.numberofseats, c.class
    from taflymodified g
    join taclass c on g.type=c.type;

/*insert data staticly into d_club*/
insert into D_CLUB (club_key, clubName,address,zipcode,region)
select sqDWH.nextval, mane, address,zipcode, region
from taclub;

/*generate d_age data*/
declare 
begin 
for i in 1..110 loop
  insert into d_age values(sqDWH.nextval,i);
end loop;
end;
/

/*insert data staaticly to the D_launchtype dimension*/
insert into d_launchtype values(sqDWH.nextval,'Y','Aerotow');
insert into d_launchtype values(sqDWH.nextval,'N','Aerotow');
insert into d_launchtype values(sqDWH.nextval,'Y','Winch');
insert into d_launchtype values(sqDWH.nextval,'N','Winch');
insert into d_launchtype values(sqDWH.nextval,'Y','Self launch');
insert into d_launchtype values(sqDWH.nextval,'N','Self launch');

commit;
